package com.ilabservice.hlslive.config;

import org.influxdb.InfluxDB;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class InfluxDbConfig {
    @Value("${spring.influx.url:'http://localhost:8086'}")
    private String influxDBUrl;
    @Value("${spring.influx.user:'ilabservice'}")
    private String userName;

    @Value("${spring.influx.password:'123qwe'}")
    private String password;

    @Value("${spring.influx.database:'ilabservice'}")
    private String database;

    @Bean
    public InfluxDbUtils influxDbUtils() {
        return new InfluxDbUtils(userName, password, influxDBUrl, database, "videoRecorderMonitor","90d");
    }

    @Bean
    public InfluxDB influxDB() {
        return new InfluxDbUtils(userName, password, influxDBUrl, database, "videoRecorderMonitor","90d").influxDbBuild();
    }
}
