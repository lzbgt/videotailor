package com.ilabservice.hlslive.repository;

import com.ilabservice.hlslive.entity.Video;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VideoRepository extends JpaRepository<Video,Long> {
    List<Video> findByVideo(String video);
}
