package com.ilabservice.hlslive.ffmpeg;

import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.builder.FFmpegBuilder;
import org.apache.tomcat.jni.Local;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FirstFrame {

    private final static Logger log = LoggerFactory.getLogger(FirstFrame.class);

    public static void main(String[] args){


        String dateTime;
        long timeLong = Long.valueOf(1513937933);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        dateTime = simpleDateFormat.format(new Date(timeLong * 1000L));



        System.out.println(dateTime);
        String video = "/Users/lijunjie/KadanLAB/data/recorde.mp4";
        String image = "/Users/lijunjie/KadanLAB/data/recorde.jpg";
        String mergeTxt= "/Users/lijunjie/KadanLAB/data/Archive/capture.txt";
        String mergeVideo = "/Users/lijunjie/KadanLAB/data/Archive/capture5.mkv";

        //new FirstFrame().getFirstFrameAsImage(video,image);
        new FirstFrame().getVideoTime("/Users/lijunjie/KadanLAB/data/C90840818/1563937933-1563938933/1563937933-1563938933.mp4","/usr/local/bin/ffmpeg");
    }

    public void getFirstFrameAsImage(String inputVideo, String image, String ffmpegpath){
        List<String> commands = new java.util.ArrayList<String>();
        commands.add(ffmpegpath);
        commands.add("-i");
        commands.add(inputVideo);
        commands.add("-ss");
        commands.add("1");
        commands.add("-f");
        commands.add("image2");
        commands.add(image);
        ProcessBuilder builder = new ProcessBuilder();
        builder.command(commands);
        try {
            final Process p = builder.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void concat(String input, String output, FFmpeg fFmpeg, FFprobe fFprobe){
        FFmpegBuilder builder =
                new FFmpegBuilder()
                        .setFormat("concat")
                        .addInput(input)
                        .addOutput(output)
                        .setVideoCodec("copy")
                        .setAudioCodec("copy")
                        .done();
        FFmpegExecutor executor = null;
        try {
            executor = new FFmpegExecutor(fFmpeg,fFprobe);
        } catch (Exception e) {
            e.printStackTrace();
        }
        executor.createJob(builder).run();
    }


    public void concat(String input, String output){
        FFmpegBuilder builder =
                new FFmpegBuilder()
                        .setFormat("concat")
                        .addInput(input)
                        .addOutput(output)
                        .setVideoCodec("copy")
                        .setAudioCodec("copy")
                        .done();
        FFmpegExecutor executor = null;
        try {
            executor = new FFmpegExecutor(new FFmpeg("/usr/local/bin/ffmpeg"),new FFprobe("/usr/local/bin/ffprobe"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        executor.createJob(builder).run();
    }


    public  int getVideoTime(String video_path, String ffmpeg_path) {
        List<String> commands = new java.util.ArrayList<String>();
        commands.add(ffmpeg_path);
        commands.add("-i");
        commands.add(video_path);
        try {
            ProcessBuilder builder = new ProcessBuilder();
            builder.command(commands);
            final Process p = builder.start();

            //从输入流中读取视频信息
            BufferedReader br = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            br.close();

            //从视频信息中解析时长
            String regexDuration = "Duration: (.*?), start: (.*?), bitrate: (\\d*) kb\\/s";
            Pattern pattern = Pattern.compile(regexDuration);
            Matcher m = pattern.matcher(sb.toString());
            if (m.find()) {
                int time = getTimelen(m.group(1));
                log.info(video_path+ ", length："+time+", start@ ："+m.group(2)+", Bits："+m.group(3)+"kb/s");
                return time;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    private int getTimelen(String timelen) {
        int min = 0;
        String strs[] = timelen.split(":");
        if (strs[0].compareTo("0") > 0) {
            min += Integer.valueOf(strs[0]) * 60 * 60;//秒
        }
        if (strs[1].compareTo("0") > 0) {
            min += Integer.valueOf(strs[1]) * 60;
        }
        if (strs[2].compareTo("0") > 0) {
            min += Math.round(Float.valueOf(strs[2]));
        }
        return min;
    }

    }
