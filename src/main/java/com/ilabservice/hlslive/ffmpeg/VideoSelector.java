package com.ilabservice.hlslive.ffmpeg;

import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.builder.FFmpegBuilder;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

//@RestController
@Deprecated
public class VideoSelector {

    @Value("${videoPath}")
    private String videoPath;

    @Value("${ffmpegpath}")
    private String ffmpegpath;

    @Value("${ffprobepath}")
    private String ffprobepath;

    @Value("${tempVideoPath}")
    private String tempVideoPath;

    @Value("${fileServer}")
    private String fileServer;

    final DateTimeFormatter fileDTF = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss");
    final DateTimeFormatter selectDTF = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm");
    final DateTimeFormatter folderDTF = DateTimeFormatter.ofPattern("yyyy-MM-dd");

//    public static void main(String args[]){
//        String st = "2019-04-28-20-36";
////        System.out.println(StringUtils.substring(st,11,13));
////        System.out.println(StringUtils.substring(st,14,16));
//        new VideoSelector().getVideos("","","");
//    }


    @RequestMapping(value="/tailorVideo",method = RequestMethod.GET)
    public String getVideos(@RequestParam("stream") String stream, @RequestParam("st") String st, @RequestParam("et") String et){

//        stream = "live";
//        st = "2019-04-27-14-26";
//        et = "2019-04-28-21-45";


        LocalDateTime startDate = LocalDateTime.parse(st, selectDTF);
        LocalDateTime endDate = LocalDateTime.parse(et, selectDTF);
        System.out.println(startDate.isBefore(endDate));

        String folder_st = st.substring(0,10);
        String folder_et = et.substring(0,10);

        LocalDate startDateFolder = LocalDate.parse(folder_st, folderDTF);
        LocalDate endDateFolder = LocalDate.parse(folder_et, folderDTF);


        List<String> selectFiles = new ArrayList<String>();


        String parentFolder = videoPath + "/" + stream;
        File parentFolders = new File(parentFolder);

        List<String> allFolders = Arrays.stream(parentFolders.listFiles())
                .filter(s->s.isDirectory())
                .map(s->s.getName())
                .filter(s->isFolderDate(s)).collect(Collectors.toList());

        allFolders.sort(new MyComparator());

        List<String> allBeforeFolder = allFolders.stream().filter(s->LocalDate.parse(s).isBefore(startDateFolder)).collect(Collectors.toList());
        List<String> allAfterFolder = allFolders.stream().filter(s->LocalDate.parse(s).isAfter(endDateFolder)).collect(Collectors.toList());


        allFolders.removeAll(allBeforeFolder);
        allFolders.removeAll(allAfterFolder);

//        allFolders.stream().forEach(s-> {
//                    System.out.println(" selected folders  ===>  " + s);
//                }
//        );

        for(int i=0;i<allFolders.size();i++){
            String currentFolder = allFolders.get(i);

            File subFolder = new File(parentFolder+"/"+currentFolder);
            selectFiles = Arrays.stream(subFolder.listFiles()).filter(s->s.isFile() && s.getName().contains(".mp4"))
                    .filter(s->isDate(s.getName()))
                    .map(s->s.getName())
                    .collect(Collectors.toList());
            selectFiles.sort(new MyComparator());
//            selectFiles.stream().forEach(s->System.out.println("all files in current folder  ===>  " + s));
            List<String> allBefore = selectFiles.stream().filter(s->LocalDateTime.parse(StringUtils.substringBefore(s,".mp4"),fileDTF).isBefore(startDate)).collect(Collectors.toList());
            List<String> allAfter = selectFiles.stream().filter(s->LocalDateTime.parse(StringUtils.substringBefore(s,".mp4"),fileDTF).isAfter(endDate)).collect(Collectors.toList());
//            allBefore.stream().forEach(s->System.out.println("all before ===>  " + s));
//            allAfter.stream().forEach(s->System.out.println("all after ===>  " + s));
            selectFiles.removeAll(allAfter);
            selectFiles.removeAll(allBefore);
            if(allBefore != null && allBefore.size()>0){
                selectFiles.add(allBefore.get(allBefore.size()-1));
                selectFiles.sort(new MyComparator());
            }
            final String temStream = stream;
            selectFiles.forEach(s-> {
                try {
                    FileUtils.copyFile(new File(videoPath+"/"+temStream+"/"+currentFolder+"/"+s),new File(tempVideoPath+"/"+temStream +"/" +s));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

            //selectFiles.stream().forEach(s->System.out.println("all selected files  ===>  " + s));
        }

        //selectFiles.stream().forEach(s->System.out.println("all selected files  ===>  " + s));

        File targetFiles = new File(tempVideoPath+"/"+stream);
        String finalSt = st;
        String finalStream = stream;
        String finalEt = et;

        List<String> manifest = new ArrayList<String>();
//            FileUtils.touch(new File(tempVideoPath+"/"+stream+"/"+stream + "-" +st + "-" + et+".txt"));
        try {
            PrintWriter out = new PrintWriter(new BufferedWriter(
                        new FileWriter(tempVideoPath+"/"+stream+"/"+stream + "-" +st + "-" + et+".txt", true)));


        AtomicInteger count = new AtomicInteger();
        List<String> targetFIles = Arrays.stream(targetFiles.listFiles()).map(s->s.getName())
                .filter(s->isDate(StringUtils.substringBefore(s,".mp4")) && s.contains(".mp4")).collect(Collectors.toList());
        targetFIles.sort(new MyComparator());
            targetFIles.forEach(s->{
            count.getAndIncrement();
            if(isDate(StringUtils.substringBefore(s,".mp4"))
                    && LocalDateTime.parse(StringUtils.substringBefore(s,".mp4"),fileDTF).isBefore(startDate)){
                int videoLength = getVideoTime(tempVideoPath+"/"+ finalStream +"/" + s,ffmpegpath);
                int startOffSet = (int) calStartOffset(finalSt,s);
                System.out.println("video length ==> " + videoLength);
                System.out.println("start offset ==> " + startOffSet);

                if(videoLength>0 && startOffSet>0 && videoLength > startOffSet) {
                    tailorVideo(tempVideoPath+"/"+ finalStream +"/" + s,tempVideoPath+"/"+ finalStream +"/tailor-" + s,startOffSet);
                }
                System.out.println("tailored video length ==> " + getVideoTime(tempVideoPath+"/"+ finalStream +"/tailor-" + s,ffmpegpath));

                out.println("file " + "tailor-" + s);
            }else if(count.get() == targetFIles.size()){
                int startOffSet = (int) calDuration(s,finalEt) + 1;
                int videoLength = getVideoTime(tempVideoPath+"/"+ finalStream +"/" + s,ffmpegpath);
                System.out.println("the last file " + s);
                System.out.println("video length ==> " + videoLength);
                System.out.println("start offset ==> " + startOffSet);

                if(videoLength>0 && startOffSet>0) {
                    tailorVideo_TO(tempVideoPath+"/"+ finalStream +"/" + s,tempVideoPath+"/"+ finalStream +"/tailor-" + s,startOffSet);
                    out.println("file " +  "tailor-" + s);
                }

            }else
                out.println("file " +  s);

        });


        if(out !=null )
            out.close();

        try {
            Files.deleteIfExists(Paths.get(tempVideoPath+"/"+stream+"/"+stream + "-" +st + "-" + et+".mp4"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        concat(tempVideoPath+"/"+stream+"/"+stream + "-" +st + "-" + et+".txt",tempVideoPath+"/"+stream+"/"+stream + "-" +st + "-" + et+".mp4");

            try {
                Files.deleteIfExists(Paths.get(tempVideoPath+"/"+stream+"/"+stream + "-" +st + "-" + et+".txt"));
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return fileServer + stream + "/"+stream + "-" +st + "-" + et+".mp4";

    }


    private class MyComparator implements Comparator<Object>

    {

        @Override
        public int compare(Object o1, Object o2) {

            String s1 = (String) o1;
            String s2 = (String) o2;

            double start = Double.valueOf(StringUtils.substringBefore(s1,".mp4").replaceAll("-",""));

            double end = Double.valueOf(StringUtils.substringBefore(s2,".mp4").replaceAll("-",""));

            if(start>end)
                return 1;
            if(start<end)
                return -1;
            else
                return 0;
        }
    }


    public boolean isDate(String input){

        DateTimeFormatter fileDTF = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss");
        try{
            LocalDateTime.parse(StringUtils.substringBefore(input,".mp4"),fileDTF);
            return true;
        }catch(Exception e){
            return false;
        }

    }

    public boolean isFolderDate(String input){

        DateTimeFormatter fileDTF = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        try{
            LocalDate.parse(input,fileDTF);
            return true;
        }catch(Exception e){
            return false;
        }

    }




    public int getVideoTime(String video_path, String ffmpeg_path) {
        List<String> commands = new java.util.ArrayList<String>();
        commands.add(ffmpeg_path);
        commands.add("-i");
        commands.add(video_path);
        try {
            ProcessBuilder builder = new ProcessBuilder();
            builder.command(commands);
            final Process p = builder.start();

            //从输入流中读取视频信息
            BufferedReader br = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            br.close();
            String regexDuration = "Duration: (.*?), start: (.*?), bitrate: (\\d*) kb\\/s";
            Pattern pattern = Pattern.compile(regexDuration);
            Matcher m = pattern.matcher(sb.toString());
            if (m.find()) {
                int time = getTimelen(m.group(1));
                //System.out.println(video_path+",duration："+time+", start："+m.group(2)+",bitrate："+m.group(3)+"kb/s");
                if(time==0) time =1;
                return time;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    //格式:"00:00:10.68"
    private int getTimelen(String timelen){
        int min=0;
        String strs[] = timelen.split(":");
        if (strs[0].compareTo("0") > 0) {
            min+=Integer.valueOf(strs[0])*60*60;//秒
        }
        if(strs[1].compareTo("0")>0){
            min+=Integer.valueOf(strs[1])*60;
        }
        if(strs[2].compareTo("0")>0){
            min+=Math.round(Float.valueOf(strs[2]));
        }

//        System.out.println(" ===> " +min/60 + " -- " + (min - min/60 * 60));

        return min/60;
    }


    public void tailorVideo(String input, String output,int startOffSet) {

        FFmpegBuilder builder =
                new FFmpegBuilder()
                        .addInput(input)
                        .setStartOffset(startOffSet, TimeUnit.MINUTES)
                        .addOutput(output)
                        .setVideoCodec("copy")
                        .setAudioCodec("copy")
                        .done();
        FFmpegExecutor executor = null;
        try {
            executor = new FFmpegExecutor(new FFmpeg(ffmpegpath),new FFprobe(ffprobepath));
        } catch (IOException e) {
            e.printStackTrace();
        }
        executor.createJob(builder).run();

    }


    public void concat(String input, String output){
            FFmpegBuilder builder =
                    new FFmpegBuilder()
                            .setFormat("concat")
                            .addInput(input)
                            .addOutput(output)
                            .setVideoCodec("copy")
                            .setAudioCodec("copy")
                            .done();
            FFmpegExecutor executor = null;
            try {
                executor = new FFmpegExecutor(new FFmpeg(ffmpegpath),new FFprobe(ffprobepath));
            } catch (IOException e) {
                e.printStackTrace();
            }
            executor.createJob(builder).run();
    }

    public void tailorVideo_TO(String input, String output,int duration) {

        FFmpegBuilder builder =
                new FFmpegBuilder()
                        .addInput(input)
                        .addOutput(output)
                        .setDuration(duration,TimeUnit.MINUTES)
                        .setVideoCodec("copy")
                        .setAudioCodec("copy")
                        .done();
        FFmpegExecutor executor = null;
        try {
            executor = new FFmpegExecutor(new FFmpeg(ffmpegpath),new FFprobe(ffprobepath));
        } catch (IOException e) {
            e.printStackTrace();
        }
        executor.createJob(builder).run();

    }


    public long calStartOffset(String st, String startFile){
        LocalDateTime startDate = LocalDateTime.parse(st, selectDTF);
        LocalDateTime fileStartDate = LocalDateTime.parse(StringUtils.substringBefore(startFile,".mp4"), fileDTF);
        Duration duration = Duration.between(fileStartDate,startDate);
        return duration.toMinutes();
    }


    public long calDuration(String endFile, String et){
        LocalDateTime endFileDate = LocalDateTime.parse(StringUtils.substringBefore(endFile,".mp4"), fileDTF);
        LocalDateTime selectEndDate = LocalDateTime.parse(et, selectDTF);
        Duration duration = Duration.between(endFileDate,selectEndDate);
        return duration.toMinutes();
    }

}
