package com.ilabservice.hlslive.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Video {
    @Column(nullable = false)
    public String video;
    @Column(nullable = true)
    public String image;
    @Column(nullable = true)
    public String startTime;
    @Column(nullable = true)
    public String endTime;
    @Column(nullable = true)
    public int length;

    @Id
    @GeneratedValue
    private Long id;


    public Video(String video, String startTime,String endTime,String image, int length){
        this.video = video;
        this.startTime = startTime;
        this.endTime = endTime;
        this.image = image;
        this.length = length;

    }
    //default constructor
    public Video(){}

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }



    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }


}